/*! \file  serialOut.c
 *
 *  \brief Send characters out from the serial output buffer
 *
 *
 *  \author jjmcd
 *  \date July 10, 2015, 9:37 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "graphicsTerminal.h"

/*! serialOut() - Send characters out from the serial output buffer */

/*! serialOut() examines \c szOutBuf and sends \c nOutLen characters
 *  out the serial port.  When complete, \c nOutLen is set to zero.
 *
 * \param none
 * \return none
 */
void serialOut(void)
{
  unsigned char *p;
  int i;

  p = szOutBuf;
  while (p < (szOutBuf + nOutLen))
    {
      while (U2STAbits.UTXBF); // Wait if buffer full
      while (!U2STAbits.TRMT); // Wait for prev char to complete
      U2TXREG = *p; // char to UART xmit register
      p++;
//      for ( i=0; i<10; i++ )  ;
    }
  nOutLen = 0;
}
