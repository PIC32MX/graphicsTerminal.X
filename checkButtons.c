/*! \file  checkButtons.c
 *
 *  \brief Check is buttons pressed and draw if necessary
 *
 *  \author jjmcd
 *  \date July 17, 2015, 3:23 PM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/buttons.h"
#define EXTERN
#include "../Button.X/buttonAttributes.h"


/*! checkButtons() - Update the state of buttons against touch data */

/*! Looks through all the buttons and checks if the current touch
 *  (if any) is within the button.  Adjusts the state of the button
 *  and if it changed, redraw the button.
 */
void checkButtons(void)
{
  int i;
  int x, y;
  int dataAvail, gotData, buttonTouched;

  /* Record that we haven't yet read the touch data */
  gotData = 0;
  /* See if there is any touch data available */
  dataAvail = touchDataAvailable();
  /* Loop through all the buttons */
  for (i = 0; i < 32; i++)
    {
      /* Assume this button isn't touched right now */
      buttonTouched = 0;
      /* Is this a button we have to consider? */
      if (rcButton[i].uIsActive)
        {
          /* If there is data available */
          if (dataAvail)
            {
              /* If we haven't yet gotten the data, get it */
              if (!gotData)
                {
                  touchRead();
                  x = touchGetX();
                  y = touchGetY();
                  gotData = 1;
                }
              /* Is the touch within this button's outline */
              if (x > rcButton[i].x1)
                if (x < rcButton[i].x2)
                  if (y > rcButton[i].y1)
                    if (y < rcButton[i].y2)
                      /* Yes, the button is currently touched */
                      buttonTouched = 1;
            } // if dataAvail
          /* If this button is touched */
          if (buttonTouched)
            {
              /* If the button wasn't previously touched */
              if (!rcButton[i].uState)
                {
                  /* Note the state change and redraw the button */
                  rcButton[i].uState = 1;
                  drawButton(i);
                }
            } // if button touched
          /* If this button wasn't touched, or there is no longer a touch */
          else
            {
              /* If the button was previously touched */
              if (rcButton[i].uState)
                {
                  /* Note the state change and redraw the button */
                  rcButton[i].uState = 0;
                  drawButton(i);
                }
            } // else button touched
        }  /* if button active */
    }  /* for i */
}
