/*! \file  checkX.c
 *
 *  \brief Check whether X within limits and limit it
 *
 *
 *  \author jjmcd
 *  \date June 22, 2015, 8:04 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#define EXTERN
#include "../include/class_attributes.h"


/*! checkX - Check whether X within limits and limit it */
/*! If the parameter is in the range for the orientation returns true,
 *  otherwise clips the parameter to within range and returns
 *  false.
 * 
 * \param x int * - Value to be tested
 * \return TRUE if within range else false
 */
int checkX( int *x )
{
  if (orient == LANDSCAPE)
    {
      if (*x > 319)
        {
          *x = 319;
          return 0;
        }
    }
  else
    {
      if (*x > 239)
        {
          *x = 239;
          return 0;
        }
    }
  if (*x < 0)
    {
      *x = 0;
      return 0;
    }
  return 1;
}
