/*! \file  fatalErrorFlash.c
 *
 *  \brief Flash the Busy LED
 *
 *
 *  \author jjmcd
 *  \date November 19, 2015, 11:29 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/Delays.h"


/*! fatalErrorFlash() - Flash the Busy LED */

/*! fatalErrorFlash() is called by the various fatal error routines.
 *  It flashes the busy LED on for 50ms and off for 100ms five
 *  times and returns.
 *
 * \param none
 * \return none
 */
void fatalErrorFlash( void )
{
  int i;
      for (i = 0; i < 5; i++)
        {
          _LATB5 = 1;
          delay(50);
          _LATB5 = 0;
          delay(100);
        }
  
}

