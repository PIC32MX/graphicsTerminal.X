/*! \file  putString.c
 *
 *  \brief Display a text string with specified spacing
 *
 *
 *  \author jjmcd
 *  \date November 21, 2015, 9:23 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>
#include "../include/TFT.h"
#include "../include/colors.h"


/*! putString() - Display a text string with specified spacing */

/*! putString() displays a character string at a specified location
 *  on the screen with specified character spacing.
 * 
 * \param x int - horizontal position for the string
 * \param y int - vertical position for the string
 * \param space int - Number of pixels between the start of each character
 * \param szMessage char * - Pointer to message to display
 * \return none
 */
void putString( int x, int y, int space, char*szMessage )
{
  char *p;
  p = szMessage;
  while ( *p )
    {
      TFTprintChar(*p,x,y);
      x+=space;
      p++;
    }
}
