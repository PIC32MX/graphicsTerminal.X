#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/graphicsTerminal.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/graphicsTerminal.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=U2RXISR.c graphicsTerminal.c setupUART.c checkX.c checkY.c processCommand.c TFTsoftwareID.c grumpyCat.c fatalError.c serialOut.c queryTouch.c checkButtons.c getButtonState.c fatalErrorFlash.c hostFatalError.c initializeButtons.c putString.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/U2RXISR.o ${OBJECTDIR}/graphicsTerminal.o ${OBJECTDIR}/setupUART.o ${OBJECTDIR}/checkX.o ${OBJECTDIR}/checkY.o ${OBJECTDIR}/processCommand.o ${OBJECTDIR}/TFTsoftwareID.o ${OBJECTDIR}/grumpyCat.o ${OBJECTDIR}/fatalError.o ${OBJECTDIR}/serialOut.o ${OBJECTDIR}/queryTouch.o ${OBJECTDIR}/checkButtons.o ${OBJECTDIR}/getButtonState.o ${OBJECTDIR}/fatalErrorFlash.o ${OBJECTDIR}/hostFatalError.o ${OBJECTDIR}/initializeButtons.o ${OBJECTDIR}/putString.o
POSSIBLE_DEPFILES=${OBJECTDIR}/U2RXISR.o.d ${OBJECTDIR}/graphicsTerminal.o.d ${OBJECTDIR}/setupUART.o.d ${OBJECTDIR}/checkX.o.d ${OBJECTDIR}/checkY.o.d ${OBJECTDIR}/processCommand.o.d ${OBJECTDIR}/TFTsoftwareID.o.d ${OBJECTDIR}/grumpyCat.o.d ${OBJECTDIR}/fatalError.o.d ${OBJECTDIR}/serialOut.o.d ${OBJECTDIR}/queryTouch.o.d ${OBJECTDIR}/checkButtons.o.d ${OBJECTDIR}/getButtonState.o.d ${OBJECTDIR}/fatalErrorFlash.o.d ${OBJECTDIR}/hostFatalError.o.d ${OBJECTDIR}/initializeButtons.o.d ${OBJECTDIR}/putString.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/U2RXISR.o ${OBJECTDIR}/graphicsTerminal.o ${OBJECTDIR}/setupUART.o ${OBJECTDIR}/checkX.o ${OBJECTDIR}/checkY.o ${OBJECTDIR}/processCommand.o ${OBJECTDIR}/TFTsoftwareID.o ${OBJECTDIR}/grumpyCat.o ${OBJECTDIR}/fatalError.o ${OBJECTDIR}/serialOut.o ${OBJECTDIR}/queryTouch.o ${OBJECTDIR}/checkButtons.o ${OBJECTDIR}/getButtonState.o ${OBJECTDIR}/fatalErrorFlash.o ${OBJECTDIR}/hostFatalError.o ${OBJECTDIR}/initializeButtons.o ${OBJECTDIR}/putString.o

# Source Files
SOURCEFILES=U2RXISR.c graphicsTerminal.c setupUART.c checkX.c checkY.c processCommand.c TFTsoftwareID.c grumpyCat.c fatalError.c serialOut.c queryTouch.c checkButtons.c getButtonState.c fatalErrorFlash.c hostFatalError.c initializeButtons.c putString.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/graphicsTerminal.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX250F128B
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/U2RXISR.o: U2RXISR.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/U2RXISR.o.d 
	@${RM} ${OBJECTDIR}/U2RXISR.o 
	@${FIXDEPS} "${OBJECTDIR}/U2RXISR.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/U2RXISR.o.d" -o ${OBJECTDIR}/U2RXISR.o U2RXISR.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/graphicsTerminal.o: graphicsTerminal.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/graphicsTerminal.o.d 
	@${RM} ${OBJECTDIR}/graphicsTerminal.o 
	@${FIXDEPS} "${OBJECTDIR}/graphicsTerminal.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/graphicsTerminal.o.d" -o ${OBJECTDIR}/graphicsTerminal.o graphicsTerminal.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/setupUART.o: setupUART.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/setupUART.o.d 
	@${RM} ${OBJECTDIR}/setupUART.o 
	@${FIXDEPS} "${OBJECTDIR}/setupUART.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/setupUART.o.d" -o ${OBJECTDIR}/setupUART.o setupUART.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/checkX.o: checkX.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/checkX.o.d 
	@${RM} ${OBJECTDIR}/checkX.o 
	@${FIXDEPS} "${OBJECTDIR}/checkX.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/checkX.o.d" -o ${OBJECTDIR}/checkX.o checkX.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/checkY.o: checkY.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/checkY.o.d 
	@${RM} ${OBJECTDIR}/checkY.o 
	@${FIXDEPS} "${OBJECTDIR}/checkY.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/checkY.o.d" -o ${OBJECTDIR}/checkY.o checkY.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/processCommand.o: processCommand.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/processCommand.o.d 
	@${RM} ${OBJECTDIR}/processCommand.o 
	@${FIXDEPS} "${OBJECTDIR}/processCommand.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/processCommand.o.d" -o ${OBJECTDIR}/processCommand.o processCommand.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/TFTsoftwareID.o: TFTsoftwareID.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTsoftwareID.o.d 
	@${RM} ${OBJECTDIR}/TFTsoftwareID.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTsoftwareID.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTsoftwareID.o.d" -o ${OBJECTDIR}/TFTsoftwareID.o TFTsoftwareID.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/grumpyCat.o: grumpyCat.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/grumpyCat.o.d 
	@${RM} ${OBJECTDIR}/grumpyCat.o 
	@${FIXDEPS} "${OBJECTDIR}/grumpyCat.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/grumpyCat.o.d" -o ${OBJECTDIR}/grumpyCat.o grumpyCat.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/fatalError.o: fatalError.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/fatalError.o.d 
	@${RM} ${OBJECTDIR}/fatalError.o 
	@${FIXDEPS} "${OBJECTDIR}/fatalError.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/fatalError.o.d" -o ${OBJECTDIR}/fatalError.o fatalError.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/serialOut.o: serialOut.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/serialOut.o.d 
	@${RM} ${OBJECTDIR}/serialOut.o 
	@${FIXDEPS} "${OBJECTDIR}/serialOut.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/serialOut.o.d" -o ${OBJECTDIR}/serialOut.o serialOut.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/queryTouch.o: queryTouch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/queryTouch.o.d 
	@${RM} ${OBJECTDIR}/queryTouch.o 
	@${FIXDEPS} "${OBJECTDIR}/queryTouch.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/queryTouch.o.d" -o ${OBJECTDIR}/queryTouch.o queryTouch.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/checkButtons.o: checkButtons.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/checkButtons.o.d 
	@${RM} ${OBJECTDIR}/checkButtons.o 
	@${FIXDEPS} "${OBJECTDIR}/checkButtons.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/checkButtons.o.d" -o ${OBJECTDIR}/checkButtons.o checkButtons.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/getButtonState.o: getButtonState.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/getButtonState.o.d 
	@${RM} ${OBJECTDIR}/getButtonState.o 
	@${FIXDEPS} "${OBJECTDIR}/getButtonState.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/getButtonState.o.d" -o ${OBJECTDIR}/getButtonState.o getButtonState.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/fatalErrorFlash.o: fatalErrorFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/fatalErrorFlash.o.d 
	@${RM} ${OBJECTDIR}/fatalErrorFlash.o 
	@${FIXDEPS} "${OBJECTDIR}/fatalErrorFlash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/fatalErrorFlash.o.d" -o ${OBJECTDIR}/fatalErrorFlash.o fatalErrorFlash.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/hostFatalError.o: hostFatalError.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/hostFatalError.o.d 
	@${RM} ${OBJECTDIR}/hostFatalError.o 
	@${FIXDEPS} "${OBJECTDIR}/hostFatalError.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/hostFatalError.o.d" -o ${OBJECTDIR}/hostFatalError.o hostFatalError.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/initializeButtons.o: initializeButtons.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/initializeButtons.o.d 
	@${RM} ${OBJECTDIR}/initializeButtons.o 
	@${FIXDEPS} "${OBJECTDIR}/initializeButtons.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/initializeButtons.o.d" -o ${OBJECTDIR}/initializeButtons.o initializeButtons.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/putString.o: putString.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/putString.o.d 
	@${RM} ${OBJECTDIR}/putString.o 
	@${FIXDEPS} "${OBJECTDIR}/putString.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/putString.o.d" -o ${OBJECTDIR}/putString.o putString.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
else
${OBJECTDIR}/U2RXISR.o: U2RXISR.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/U2RXISR.o.d 
	@${RM} ${OBJECTDIR}/U2RXISR.o 
	@${FIXDEPS} "${OBJECTDIR}/U2RXISR.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/U2RXISR.o.d" -o ${OBJECTDIR}/U2RXISR.o U2RXISR.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/graphicsTerminal.o: graphicsTerminal.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/graphicsTerminal.o.d 
	@${RM} ${OBJECTDIR}/graphicsTerminal.o 
	@${FIXDEPS} "${OBJECTDIR}/graphicsTerminal.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/graphicsTerminal.o.d" -o ${OBJECTDIR}/graphicsTerminal.o graphicsTerminal.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/setupUART.o: setupUART.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/setupUART.o.d 
	@${RM} ${OBJECTDIR}/setupUART.o 
	@${FIXDEPS} "${OBJECTDIR}/setupUART.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/setupUART.o.d" -o ${OBJECTDIR}/setupUART.o setupUART.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/checkX.o: checkX.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/checkX.o.d 
	@${RM} ${OBJECTDIR}/checkX.o 
	@${FIXDEPS} "${OBJECTDIR}/checkX.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/checkX.o.d" -o ${OBJECTDIR}/checkX.o checkX.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/checkY.o: checkY.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/checkY.o.d 
	@${RM} ${OBJECTDIR}/checkY.o 
	@${FIXDEPS} "${OBJECTDIR}/checkY.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/checkY.o.d" -o ${OBJECTDIR}/checkY.o checkY.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/processCommand.o: processCommand.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/processCommand.o.d 
	@${RM} ${OBJECTDIR}/processCommand.o 
	@${FIXDEPS} "${OBJECTDIR}/processCommand.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/processCommand.o.d" -o ${OBJECTDIR}/processCommand.o processCommand.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/TFTsoftwareID.o: TFTsoftwareID.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTsoftwareID.o.d 
	@${RM} ${OBJECTDIR}/TFTsoftwareID.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTsoftwareID.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTsoftwareID.o.d" -o ${OBJECTDIR}/TFTsoftwareID.o TFTsoftwareID.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/grumpyCat.o: grumpyCat.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/grumpyCat.o.d 
	@${RM} ${OBJECTDIR}/grumpyCat.o 
	@${FIXDEPS} "${OBJECTDIR}/grumpyCat.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/grumpyCat.o.d" -o ${OBJECTDIR}/grumpyCat.o grumpyCat.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/fatalError.o: fatalError.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/fatalError.o.d 
	@${RM} ${OBJECTDIR}/fatalError.o 
	@${FIXDEPS} "${OBJECTDIR}/fatalError.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/fatalError.o.d" -o ${OBJECTDIR}/fatalError.o fatalError.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/serialOut.o: serialOut.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/serialOut.o.d 
	@${RM} ${OBJECTDIR}/serialOut.o 
	@${FIXDEPS} "${OBJECTDIR}/serialOut.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/serialOut.o.d" -o ${OBJECTDIR}/serialOut.o serialOut.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/queryTouch.o: queryTouch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/queryTouch.o.d 
	@${RM} ${OBJECTDIR}/queryTouch.o 
	@${FIXDEPS} "${OBJECTDIR}/queryTouch.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/queryTouch.o.d" -o ${OBJECTDIR}/queryTouch.o queryTouch.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/checkButtons.o: checkButtons.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/checkButtons.o.d 
	@${RM} ${OBJECTDIR}/checkButtons.o 
	@${FIXDEPS} "${OBJECTDIR}/checkButtons.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/checkButtons.o.d" -o ${OBJECTDIR}/checkButtons.o checkButtons.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/getButtonState.o: getButtonState.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/getButtonState.o.d 
	@${RM} ${OBJECTDIR}/getButtonState.o 
	@${FIXDEPS} "${OBJECTDIR}/getButtonState.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/getButtonState.o.d" -o ${OBJECTDIR}/getButtonState.o getButtonState.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/fatalErrorFlash.o: fatalErrorFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/fatalErrorFlash.o.d 
	@${RM} ${OBJECTDIR}/fatalErrorFlash.o 
	@${FIXDEPS} "${OBJECTDIR}/fatalErrorFlash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/fatalErrorFlash.o.d" -o ${OBJECTDIR}/fatalErrorFlash.o fatalErrorFlash.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/hostFatalError.o: hostFatalError.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/hostFatalError.o.d 
	@${RM} ${OBJECTDIR}/hostFatalError.o 
	@${FIXDEPS} "${OBJECTDIR}/hostFatalError.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/hostFatalError.o.d" -o ${OBJECTDIR}/hostFatalError.o hostFatalError.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/initializeButtons.o: initializeButtons.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/initializeButtons.o.d 
	@${RM} ${OBJECTDIR}/initializeButtons.o 
	@${FIXDEPS} "${OBJECTDIR}/initializeButtons.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/initializeButtons.o.d" -o ${OBJECTDIR}/initializeButtons.o initializeButtons.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
${OBJECTDIR}/putString.o: putString.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/putString.o.d 
	@${RM} ${OBJECTDIR}/putString.o 
	@${FIXDEPS} "${OBJECTDIR}/putString.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/putString.o.d" -o ${OBJECTDIR}/putString.o putString.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wextra
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/graphicsTerminal.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../lib/TFT.X.a ../lib/Touch.X.a ../lib/Button.X.a  
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mdebugger -D__MPLAB_DEBUGGER_ICD3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/graphicsTerminal.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ../lib/TFT.X.a ../lib/Touch.X.a ../lib/Button.X.a      -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    -mreserve=boot@0x1FC00490:0x1FC00BEF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD3=1,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/graphicsTerminal.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../lib/TFT.X.a ../lib/Touch.X.a ../lib/Button.X.a 
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/graphicsTerminal.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ../lib/TFT.X.a ../lib/Touch.X.a ../lib/Button.X.a      -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	${MP_CC_DIR}/xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/graphicsTerminal.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
