/*! \file  setupUART.c
 *
 *  \brief Initialize the UART
 *
 *  Sets the UART interrupt priority to 4, then maps UART2 to
 *  RPB8 and RPB9.  Disables most fancy features of the UART
 *  including CTS/RTS, sets the baud rate, then turns on the UART.
 *
 *  \author jjmcd
 *  \date March 10, 2014, 12:47 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 */
#include <xc.h>
#include "../include/pps.h"

/*! Crystal frequency */
#define XTALFRQ         20000000
/*! PLL input divider */
#define FPLLDIV         4
/*! PLL multiplier */
#define FPLLMUL         20
/*! PLL output divider */
#define FPLLODIV        2
/*! Peripheral bus divider */
#define FPBDIV          1
//! Instruction Cycle Frequency
#define FCY             ((((XTALFRQ/FPLLDIV)*FPLLMUL)/FPLLODIV)/FPBDIV)
//! Desired baud rate
#define BAUDRATE        38400
//! Value for the baud rate generator
#define BRGVAL          ((FCY/BAUDRATE)/16)-1
//! Alternate baud rate
#define BAUDRATESLOW    9600
//! Alternate value for the baud rate generator
#define BRGVALSLOW      ((FCY/BAUDRATESLOW)/16)-1

/*! setupUART() - Initialize the UART */
/*! Sets the UART interrupt priority to 4, then maps UART2 to
 *  RPB8 and RPB9.  Disables most fancy features of the UART
 *  including CTS/RTS, sets the baud rate, then turns on the UART.
 *
 * \param none
 * \return none
 */
void setupUART(void)
{
    /* Set UART interrupt priority and sub-priority */
    IPC9bits.U2IP = 4;       /* UART 2 interrupt priority to 4 */
    IPC9bits.U2IS = 2;       /* UART 2 interupt sub-priority */

    /* Map U2RX to RPB8 */
    PPSInput(2,U2RX,RPB8);
    /* Map U2TX to RPB9 */
    _TRISB9 = 0;
    ODCBbits.ODCB9 = 0;
    PPSOutput(4,RPB9,U2TX);

    /* Configure the UART */
    U2MODEbits.ON     = 0;   /* UART off */
    U2MODEbits.SIDL   = 0;   /* Continue in idle mode */
    U2MODEbits.IREN   = 0;   /* IrDA disabled */
    U2MODEbits.RTSMD  = 1;   /* U2RTS is in simplex mode */
    U2MODEbits.UEN    = 0;   /* U2TX/U2RX used, U2CTS/U2RTS port controlled */
    U2MODEbits.WAKE   = 1;   /* Wake on start bit enabled */
    U2MODEbits.LPBACK = 0;   /* Loopback disabled */
    U2MODEbits.ABAUD  = 0;   /* Autobaud disabled */
    if ( _RB3 )
      U2MODEbits.RXINV  = 0; /* U2RX idle state is '1' */
    else
      U2MODEbits.RXINV  = 1; /* U2RX idle state is '0' */
    U2MODEbits.BRGH   = 0;   /* Standard speed mode */
    U2MODEbits.PDSEL  = 0;   /* 8-bit data no parity */
    U2MODEbits.STSEL  = 0;   /* 1 stop bits */

    /* Baud rate */
    if ( _RB2 )
      U2BRG = BRGVAL;
    else
      U2BRG = BRGVALSLOW;
    //U2BRG = 81;   /* 38400 */
    //U2BRG = 162;  /* 19200 */
    //U2BRG = 324;  /* 9600 */
    //U2BRG = 1300; /* 2400 */

    /* Take care of status bits */
    U2STAbits.UTXEN   = 1;   /* Enable transmit */
    U2STAbits.URXEN   = 1;   /* Enable rcv */
    U2STAbits.URXISEL = 0;   /* Interrupt on at least 1 character */
    U2STAbits.OERR    = 0;   /* Clear overrun bit */

    /* Now we can turn the UART on */
    U2MODEbits.ON     = 1; /* UART on */
}
