/*! \file  processCommand.c
 *
 *  \brief Process one TFT function
 *
 *
 *  \author jjmcd
 *  \date June 22, 2015, 8:13 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/class_attributes.h"
#include "../include/TFT.h"
#include "../include/Touch.h"
#include "../include/buttons.h"
#include "graphicsTerminal.h"


/*! processCommand() - Process one TFT function - data in szCommand */
/*! processCommand looks at the data in the global string \c szCommand.
 *  The first byte contains a command which has an associated number
 *  of following bytes.  processCommand() collects these bytes and
 *  calls the appropriate routine to perform the TFT action.
 * 
 * \param none
 * \return none
 *
 */
void processCommand()
{
  unsigned char r,g,b,c;
  unsigned int x1,x2,y1,y2;
  unsigned int ctit,cann,cxlab,cylab,cscreen,cfield,cdata;
  int n1,n2;
  char szString[256];
  char *p;

  /* Variable length functions will have an ID >0x7f */
  if ( szCommand[0]&0x80 )
    {
      switch ( szCommand[0] )
	{
	case 0x82: /* TFTprint */
          p = &szCommand[1];
          while( *p )
            {
              TFTputchTT( *p );
              p++;
            }
          break;
	default:
	  break;
	}
      return; /* Don't handle strings yet */
    }

  switch (szCommand[0])
    {
    case 0x00:
      break;
    case 0x01: //TFTinit
      SetupHardware();
      x1 = (int)szCommand[1]; // Orientation
      x1 &= 1; // Limit to 0, 1, only valid values
      TFTinit( x1 );
      touchInit( x1 );
      touchSetPrecision( 2 ); // 1 lots of errors, 3 slow
      _transparent = 0;  // Note that _transparent=1 is extremely slow
      break;
    case 0x02:  //TFTsetColor
      r = szCommand[1];
      g = szCommand[2];
      b = szCommand[3];
      TFTsetColor(r,g,b);
      break;
    case 0x03:  //TFTsetBackColor
      r = szCommand[1];
      g = szCommand[2];
      b = szCommand[3];
      TFTsetBackColor(r,g,b);
      break;
    case 0x04: // TFTsetColorX
      x1 = szCommand[1] | szCommand[2]<<8;
      TFTsetColorX( x1 );
      break;
    case 0x05: // TFTsetBackColorX
      x1 = szCommand[1] | szCommand[2]<<8;
      TFTsetBackColorX( x1 );
      break;
    case 0x06:  // TFTclear
      TFTclear();
      nCurRow = nCurCol = 0;
      //TFTroundRect(10,220,2,230);
      break;
    case 0x07:
      TFTputchTT( szCommand[1] );
      break;
    case 0x08:  // TFTrect
      x1 = szCommand[1] | szCommand[2]<<8;
      y1 = szCommand[3] | szCommand[4]<<8;
      x2 = szCommand[5] | szCommand[6]<<8;
      y2 = szCommand[7] | szCommand[8]<<8;
      checkX(&x1);
      checkX(&x2);
      checkY(&y1);
      checkY(&y2);
      TFTrect(x1,y1,x2,y2);
      break;
    case 0x09:  // TFTclearRect
      r = szCommand[1];
      g = szCommand[2];
      b = szCommand[3];
      checkX(&x1);
      checkX(&x2);
      checkY(&y1);
      checkY(&y2);
      x1 = szCommand[4] | szCommand[5]<<8;
      y1 = szCommand[6] | szCommand[7]<<8;
      x2 = szCommand[8] | szCommand[9]<<8;
      y2 = szCommand[10] | szCommand[11]<<8;
      TFTclearRect( r, g, b, x1, y1, x2, y2 );
      break;
    case 0x0a:  // TFTpixel
      x1 = szCommand[1] | szCommand[2]<<8;
      y1 = szCommand[3] | szCommand[4]<<8;
      checkX(&x1);
      checkY(&y1);
      TFTpixel(x1,y1);
      break;
    case 0x0b: // TFTline
      x1 = szCommand[1] | szCommand[2]<<8;
      y1 = szCommand[3] | szCommand[4]<<8;
      x2 = szCommand[5] | szCommand[6]<<8;
      y2 = szCommand[7] | szCommand[8]<<8;
      checkX(&x1);
      checkX(&x2);
      checkY(&y1);
      checkY(&y2);
      TFTline( x1, y1, x2, y2 );
      break;
    case 0x0c: // TFTsetFont
      nCurFont = (int)szCommand[1];
      switch( szCommand[1] )
        {
	case 2:
	  TFTsetFont((fontdatatype*)BigFont);
	  break;
	case 3:
	  TFTsetFont((fontdatatype*)SevenSegNumFont);
	  break;
	case 4:
	  TFTsetFont((fontdatatype*)DJS);
	  break;
	case 5:
	  TFTsetFont((fontdatatype*)DJSB);
	  break;
	case 6:
	  TFTsetFont((fontdatatype*)BoldSmall);
	  break;
	case 1:
	default:
	  TFTsetFont((fontdatatype*)SmallFont);
	  break;
        }
      break;
    case 0x0d:    // TFTprintChar
      x1 = szCommand[2] | szCommand[3]<<8;
      y1 = szCommand[4] | szCommand[5]<<8;
      checkX(&x1);
      checkY(&y1);
      TFTprintChar( szCommand[1], x1, y1 );
      break;
    case 0x0e:   // TFTroundRect
      x1 = szCommand[1] | szCommand[2]<<8;
      y1 = szCommand[3] | szCommand[4]<<8;
      x2 = szCommand[5] | szCommand[6]<<8;
      y2 = szCommand[7] | szCommand[8]<<8;
      checkX(&x1);
      checkX(&x2);
      checkY(&y1);
      checkY(&y2);
      TFTroundRect(x1,y1,x2,y2);
      break;
    case 0x0f:   // TFTfillRoundRect
      x1 = szCommand[1] | szCommand[2]<<8;
      y1 = szCommand[3] | szCommand[4]<<8;
      x2 = szCommand[5] | szCommand[6]<<8;
      y2 = szCommand[7] | szCommand[8]<<8;
      checkX(&x1);
      checkX(&x2);
      checkY(&y1);
      checkY(&y2);
      TFTfillRoundRect(x1,y1,x2,y2);
      break;
    case 0x10:   // TFTcircle
      x1 = szCommand[1] | szCommand[2]<<8;
      y1 = szCommand[3] | szCommand[4]<<8;
      x2 = szCommand[5] | szCommand[6]<<8;
      checkX(&x1);
      checkY(&x2); // sic
      checkY(&y1);
      TFTcircle(x1,y1,x2);
      break;
    case 0x11:   // TFTfillCircle
      x1 = szCommand[1] | szCommand[2]<<8;
      y1 = szCommand[3] | szCommand[4]<<8;
      x2 = szCommand[5] | szCommand[6]<<8;
      TFTfillCircle(x1,y1,x2);
      break;
      case 0x12:    // TFTfillRect
      x1 = szCommand[1] | szCommand[2]<<8;
      y1 = szCommand[3] | szCommand[4]<<8;
      x2 = szCommand[5] | szCommand[6]<<8;
      y2 = szCommand[7] | szCommand[8]<<8;
      checkX(&x1);
      checkX(&x2);
      checkY(&y1);
      checkY(&y2);
      TFTfillRect(x1,y1,x2,y2);
      break;
    case 0x13:    // TFTprintDec
      n1 = szCommand[1] | szCommand[2]<<8;
      n2 = szCommand[3];
      x1 = szCommand[5] | szCommand[6]<<8;
      y1 = szCommand[7] | szCommand[8]<<8;
      checkX(&x1);
      checkY(&y1);
      TFTprintDec(n1,n2,x1,y1);
      break;
    case 0x14:    // TFTprintDecRight
      n1 = szCommand[1] | szCommand[2]<<8;
      n2 = szCommand[3];
      x1 = szCommand[5] | szCommand[6]<<8;
      y1 = szCommand[7] | szCommand[8]<<8;
      checkX(&x1);
      checkY(&y1);
      TFTprintDecRight(n1,n2,x1,y1);
      break;
    case 0x15:    // TFTprintInt
      n1 = szCommand[1] | szCommand[2]<<8;
      n2 = szCommand[3];
      x1 = szCommand[5] | szCommand[6]<<8;
      y1 = szCommand[7] | szCommand[8]<<8;
      checkX(&x1);
      checkY(&y1);
      TFTprintInt(n1,n2,x1,y1);
      break;
    case 0x16:    // TFTtransitionOpen
      n1 = szCommand[1] | szCommand[2]<<8;
      TFTtransitionOpen(n1);
      break;
    case 0x17:    // TFTtransitionClose
      n1 = szCommand[1] | szCommand[2]<<8;
      TFTtransitionClose(n1);
      break;
    case 0x18:    // TFTtransitionWipe
      n1 = szCommand[1] | szCommand[2]<<8;
      n2 = szCommand[3] | szCommand[4]<<8;
      TFTtransitionWipe(n1,n2);
      break;
    case 0x19:      // TFTGINIT
      TFTGinit();
      break;
    case 0x1a:      // TFTGADDPT
      x1 = szCommand[1] | szCommand[2]<<8;
      TFTGaddPoint(x1);
      break;
    case 0x1b:      // TFTGEXPOSE
      TFTGexpose();
      break;
    case 0x1c:      // TFTGCOLORS
      ctit    = szCommand[ 1] | szCommand[ 2]<<8;
      cann    = szCommand[ 3] | szCommand[ 4]<<8;
      cxlab   = szCommand[ 5] | szCommand[ 6]<<8;
      cylab   = szCommand[ 7] | szCommand[ 8]<<8;
      cscreen = szCommand[ 9] | szCommand[10]<<8;
      cfield  = szCommand[11] | szCommand[12]<<8;
      cdata   = szCommand[13] | szCommand[14]<<8;
      TFTGcolors(ctit,cann,cxlab,cylab,cscreen,cfield,cdata);
      break;
    case 0x1d:
      x1 = szCommand[1] | szCommand[2]<<8;
      x2 = szCommand[3] | szCommand[4]<<8;
      y1 = szCommand[5] | szCommand[6]<<8;
      y2 = szCommand[7] | szCommand[8]<<8;
      TFTGrange(x1,x2,y1,y2);
      break;
    case 0x1e:      // TFTGTITLE
      TFTGtitle(&szCommand[1]);
      break;
    case 0x1f:      // TFTGXLABEL
      TFTGXlabel(&szCommand[1]);
      break;
    case 0x20:      // TFTGYLABEL
      TFTGYlabel(&szCommand[1]);
      break;
    case 0x21:      // TFTGNOCURSOR
      TFTGnoCursor();
      break;
    case 0x22:      // TFTGNOERASE
      TFTGnoErase();
      break;
    case 0x23:
      TFTsoftwareID(__DATE__,__TIME__,COMMIT_ID);
      break;
    case 0x24:      // TFTERASERECT
      x1 = szCommand[1] | szCommand[2]<<8;
      y1 = szCommand[3] | szCommand[4]<<8;
      x2 = szCommand[5] | szCommand[6]<<8;
      y2 = szCommand[7] | szCommand[8]<<8;
      TFTeraseRect(x1,y1,x2,y2);
      break;
    case 0x25:
      hostFatalError();
      break;
    case 0x26:      // TFTTOUCHINIT
      touchInit(1);
      break;
    case 0x27:    // TFTTOUCHQUERY
      queryTouch();
      break;
    case 0x29:      // TFTBUTMAKE1
      n1 = szCommand[1];
      x1 = szCommand[2] | szCommand[3]<<8;
      y1 = szCommand[4] | szCommand[5]<<8;
      x2 = szCommand[6] | szCommand[7]<<8;
      y2 = szCommand[8] | szCommand[9]<<8;
      makeButtonS(n1,x1,y1,x2,y2,&szCommand[10]);
      showButton(n1);
      break;
    case 0x2a:      // TFTBUTSTATE
      n1 = szCommand[1];
      getButtonState(n1);
      break;
    case 0x82:  // TFTstringTT
      p = &szCommand[1];
      TFTputsTT(p);
      break;

    default:
      //fatalErrorE(56);
      break;
    }
}
