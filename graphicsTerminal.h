/*! \file  graphicsTerminal.h
 *
 *  \brief Constants and function prototypes for graphicsTerminal
 *
 *
 *  \author jjmcd
 *  \date March 10, 2014, 8:24 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 */

#ifndef GRAPHICSTERMINAL_H
#define	GRAPHICSTERMINAL_H

#ifdef	__cplusplus
extern "C" {
#endif

#ifndef EXTERN
#define EXTERN extern
#endif

/*! Commit hash of last release */
#define COMMIT_ID "43b0dd09c71fd632819c29f41bb99500eea52713"
#define USER_ID   0x55dc
#define RELNUMBER "Release 10d" /*!< Current release number */
#define BUFFERSIZE 16384    /*!< Size of input buffer */
#define BUFFERWARN 14746    /*!< 90% full */
#define BUFFERFATAL 16382   /*!< Totally giving up */
#define BUFFERXOFF 12288    /*!< Time to send X-off */
#define BUFFERXON 4096      /*!< Time to send X-on */
#define NUMCOMMANDS 0x2b    /*!< Number of commands with fixed length */

/*! Buffer for incoming characters */
EXTERN unsigned char szBuffer[BUFFERSIZE];
/*! Position to place the next character in the buffer */
EXTERN unsigned char *pNextIn;
/*! Position in the buffer of the next character to process */
EXTERN unsigned char *pNextOut;
/*! Pointer to the end of the buffer */
EXTERN unsigned char *pEndBuffer;
/*! Buffer for command currently under consideration */
EXTERN unsigned char szCommand[512];
/* Start and end of the input buffer */
EXTERN int nStart, nEnd, nBufLen;
/*! Serial Output Buffer */
EXTERN unsigned char szOutBuf[32];
/*! Number of characters in output buffer */
EXTERN int nOutLen;

/*! Pointer to current position within command buffer */
EXTERN unsigned char *pCommand;
/*! Pointer to search for end of input buffer */
EXTERN unsigned char *pTmp;
/*! Remember whether X-fof sent */
EXTERN int sentXoff;

/***** NOTE - in TFT.X/TFTputchTT so extern not EXTERN */
/*! Current font number */
extern int nCurFont;
/*! Current TTY column and row */
extern int nCurRow,nCurCol;
/*******************************************************/

/*! hostFatalError() - Display fatal error detected by host */
void hostFatalError(void);
/*! fatalError() - Displays a fatal error notification and halts */
void fatalError(int);
/*! fatalErrorFlash() - Flash the Busy LED */
void fatalErrorFlash(void);
/*! queryTouch() - Send location of last touch */
void queryTouch( void );
/*! serialOut() - Send characters out from the serial output buffer */
void serialOut( void );
/*! processCommand() - Process one TFT function - data in szCommand */
void processCommand( void );
/*! checkButtons() - Update the state of buttons against touch data */
void checkButtons( void );

/*! delay() - Delay a specified number of milliseconds */
/*! Loops for the specified number of milliseconds, dependent on
 *  the cycle time in FCY
 *
 * \param time long - Number of milliseconds to delay
 */
int delay( long time );

/*! checkX - Check whether X within limits and limit it */
int checkX( int * );
/*! checkY - Check whether Y within limits and limit it */
int checkY( int * );
/*! initializeButtons() - Initialize the button states */
void initializeButtons( void );
/*! getButtonState - Check the state of a button */
void getButtonState(int);
/*! grumpyCat -  Draw the grumpy cat on the screen */
void grumpyCat( int, int );
/*! setupUART() - Initialize the UART */
void setupUART( void );
/*! TFTsoftwareID() - Display a screen showing specific firmware version */
void TFTsoftwareID(char *, char *, char * );

#ifdef	__cplusplus
}
#endif

#endif	/* GRAPHICSTERMINAL_H */

