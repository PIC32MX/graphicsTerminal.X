/*! \file  initializeButtons.c
 *
 *  \brief Initialize the button states
 *
 *
 *  \author jjmcd
 *  \date November 20, 2015, 9:31 PM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <string.h>
#include "../include/buttons.h"
#define EXTERN
#include "../Button.X/buttonAttributes.h"

/*! initializeButtons() - Initialize the button states */
/*! Sets the array of button states to all unused
 * 
 * \param none
 * \return none
 */
void initializeButtons( void )
{
  int i;

  for ( i=0; i<32; i++ )
    {
      memset(&rcButton[i],0,sizeof(struct stButton));
    }
  nn = 0;
}
