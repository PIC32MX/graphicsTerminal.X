## Serial Graphics Terminal

This project implements a serial graphics terminal intended to be used
as a convenient output device for embedded projects.  The project
relies on a SainSmart 20-011-918 TFT.  The project as originally
designed uses a `PIC32MX250F128B` microcontroller, however a
`PIC32MX150F128B` can be dropped in its place as a slightly less
expensive alternative.

The project communicates with its host via async serial at CMOS levels
(TTL levels are tolerated), at 38K4 baud, although a jumper is
provided to allow 9K6.  Power is provided through a micro-USB
connector or from the host.  The project requires reasonably clean 5
volts.

Related links:
* Depends on library https://gitlab.com/PIC32MX/TFT.X
* Board design at https://gitlab.com/PIC32MX/tft-board-pcb
* Line protocol described at https://gitlab.com/PIC32MX/Wire_Protocol

## MCP23S17 to TFT connections

The following diagram shows how the TFT is driven by the two
MCP23S17 I/O expanders:

![MCP23S17 I/O Expanders](images/TFT-23S17.png)


\image latex TFT-23S17.png "MCP23S17 to TFT connections" height=3in


----
