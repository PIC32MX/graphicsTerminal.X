/*! \file  queryTouch.c
 *
 *  \brief Send location of last touch
 *
 *
 *  \author jjmcd
 *  \date July 10, 2015, 9:47 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "graphicsTerminal.h"
#include "../include/Touch.h"


/*! queryTouch() - Send location of last touch */

/*! queryTouch() tests whether touch data is available.  If not, it
 *  sends four \c 0xff bytes to the serial port.  If touch data is
 *  available, sends the x and y coordinates, two bytes each.
 * 
 * \param none
 * \return none
 *
 */
void queryTouch(void)
{
  unsigned int x,y;

  if ( touchDataAvailable() )
    {
      touchRead();
      x = touchGetX();
      y = touchGetY();
      szOutBuf[0]=x&0x00ff;
      szOutBuf[1]=(x>>8)&0x00ff;
      szOutBuf[2]=y&0x00ff;
      szOutBuf[3]=(y>>8)&0x00ff;
    }
  else
    szOutBuf[0]=szOutBuf[1]=szOutBuf[2]=szOutBuf[3]=0xff;
  nOutLen = 4;
  serialOut();
}
