/*! \file  graphicsTerminal.c
 *
 *  \brief Serial graphics terminal with SainSmart TFT
 *
 *
 *  \author jjmcd
 *  \date February 28, 2014, 9:04 AM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included materials for a complete description.
 */

#include <xc.h>
#include <sys/attribs.h>
#include <string.h>
#include "../include/colors.h"
#include "../include/Delays.h"
#include "../include/TFT.h"
#define EXTERN
#include "graphicsTerminal.h"
#include "../include/ConfigBits.h"
#include "../include/class_attributes.h"

/* Lengths of the individual commands */
unsigned int cLens[NUMCOMMANDS] = {
   1,  2, 4, 4, 3, 3, 1, 2,  9, 12, 5, 9,  2, 6,  9,  9,
   7,  7, 9, 9, 9, 9, 3, 3,  5,  1, 3, 1, 15, 9, 25, 25,
  25,  1, 1, 1, 9, 1, 2, 1, 17, 34, 2 };

/*! main - Serial Graphics Terminal */

/*! Accepts commands over the serial port and displays result
 *  on the TFT.
 */
int main(int argc, char *argv[])
{
  unsigned char ch;
  int i,j,len;
  int nBufLen;

  /* Make LED an output and turn it on */
  _TRISB5 = 0;
  _LATB5=1;
  /* Make RB2/3 digital inputs with pull-ups enabled */
  ANSELBbits.ANSB2 = 0;
  ANSELBbits.ANSB3 = 0;
  CNPUBbits.CNPUB2 = 1;
  CNPUBbits.CNPUB3 = 1;

  for ( i=0; i<4; i++ )
    {
      Buffer1[i]=0L;
      Buffer2[i] = 0L;
      Buffer3[i]=0L;
    }

  /* Give some time after start before going to work */
  delay(500);

  SetupHardware();

  /* Initialize UART2 */
  setupUART();

  /* Initialize the TFT and clear */
  TFTinit( LANDSCAPE );
  TFTsetBackColorX(WHITE);
  TFTclear();

  /* This bit is really unnecessary, it is only here to let me
   * know that programming is completed and the code to be
   * tested has actually begun.  (The stupid PICkit insists on
   * programming all 128Kb of flash so it takes a while.)
   */
  delay(500);
  grumpyCat(263,1);
  TFTsetFont((fontdatatype*)SmallFont);
  TFTsetColorX(SADDLEBROWN);
  TFTprint(RELNUMBER, CENTER, 60, 0 );
  TFTsetFont((fontdatatype *)BigFont);
  TFTsetColorX(TOMATO);
  TFTprint("Prepare to", CENTER, 110, 0 );
  TFTsetColorX(FIREBRICK);
  TFTprint("Start", CENTER, 130, 0 );
  nCurFont = 1; // default to small
  if (!_RB3 )
    {
      TFTsetColorX(BLACK);
      TFTsetBackColorX(YELLOW);
    }
  else
      TFTsetColorX(BROWN);
  if ( _RB2 )
    {
        TFTprint(" 38K4 ", CENTER, 180, 0 );
    }
  else
    {
        TFTprint(" 9K6 ", CENTER, 180, 0 );
    }
  nCurRow=nCurCol=0;
  delay( 1000 );

  /* Discard chars that may be in buffer */
  for ( i=0; i<30; i++ )
    j = U2RXREG;

  /* Initialize the buffer pointers */
  pNextIn = szBuffer;
  pNextOut = szBuffer;
  pEndBuffer = szBuffer+BUFFERSIZE-1;

  TFTsetBackColorX(BLACK);
  TFTclear();
  TFTsetColorX(LAWNGREEN);
  TFTprint("GO", CENTER, 120, 0 );
  TFTsetFont((fontdatatype*)SmallFont);

  IEC1bits.U2RXIE   = 1;   /* Enable receive interrupt */
  INTEnableSystemMultiVectoredInt();
  initializeButtons();
  sentXoff = 0;
  _LATB5=0;

  while (1)
    {
      nBufLen = pNextIn - pNextOut;
      if ( nBufLen < 0 )
        nBufLen += BUFFERSIZE;
      if ( nBufLen > 0 )
        _LATB5 = 1;
      else
        _LATB5 = 0;
      while ( pNextIn == pNextOut )
        {
//          checkButtons();
        }
      if ( nBufLen > BUFFERFATAL )
        fatalError(75);
      if ( !sentXoff )
        if ( nBufLen > BUFFERXOFF )
          {
            szOutBuf[0] = 0x13;     /* DC3=^S */
            nOutLen = 1;
            serialOut();
            sentXoff = 1;
          }
      if ( sentXoff )
        if ( nBufLen < BUFFERXON )
          {
            szOutBuf[0] = 0x11;     /* DC1=^Q */
            nOutLen = 1;
            serialOut();
            sentXoff = 0;
          }
//      if ( nBufLen > BUFFERWARN )
//        {
//            _LATB5 = 1;
//        }
//      else
//        _LATB5 = 0;
      /* This is the length of the command.  print not included yet */
      if ( *pNextOut&0x80 )
        if ( *pNextOut == 0x82 )
          {
            delay(28); // Wait for more characters in the buffer
            pTmp = pNextOut+1;
            len = 1;
            while ( *pTmp )
              {
                len++;
                pTmp++;
                if ( pTmp>pEndBuffer )
                  pTmp = szBuffer;
              }
            len++; // Be sure the trailing null included
          }
        else
	  len = 1;
      else
	{
	  if ( *pNextOut<NUMCOMMANDS)
            len=cLens[*pNextOut];
	  else
            len = 1;
	}
      /* Start the command pointer at the start of the command buffer */
      pCommand = szCommand;
      /* Clearing out the buffer is overkill but handy when debugging */
      memset(szCommand,0,sizeof(szCommand));
      /* Move the command from the input buffer.  For now this
       * doesn't mean much, but once we are taking data, we will want
       * to do this with interrupts turned off so we can adjust the
       * buffer pointers for a clean next read.  But we don't want to
       * do much more with interrupts off or we will loose characters.
       */
      for ( j=0; j<len; j++ )
	{
	  while ( pNextIn == pNextOut )
	    ;
	  *pCommand++ = *pNextOut++;
          if ( pNextOut>pEndBuffer )
	    pNextOut=szBuffer;
          //showStatus(HOTPINK);
	}
      /* Now actually go manipulate the display */
      processCommand();
      for ( i=0; i<4; i++ )
        if (Buffer2[i]!=0L)
          fatalError(i);
    }

  return 0;
}

