/*! \file  grumpyCat.c
 *
 *  \brief Draw the grumpy cat on the screen
 *
 *
 *  \author jjmcd
 *  \date June 28, 2015, 11:55 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include "grumpyCat4.h"

/*! grumpyCat -  Draw the grumpy cat on the screen */

/*! grumpyCat() - Draws the grumpy cat on the screen at
 *  the specified location
 * 
 * \param x int - Horizontal location of upper left corner of image
 * \param y int - Vertical location of upper left corner of image
 * \return none
 */
void grumpyCat(int x, int y )
{
  int i, j;

  clearCS();
  setXY(x, y, x + 55, y + 55);
  for (i = 0; i < 56; i++)
    for (j = 0; j < 56; j++)
      LCD_Write_DATA16(Grumpy[j][i]); // rrrrrggggggbbbbb
  setCS();
  clrXY();
}
