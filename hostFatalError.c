/*! \file  hostFatalError.c
 *
 *  \brief Display fatal error directed by remote host
 *
 *
 *  \author jjmcd
 *  \date November 19, 2015, 11:27 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/TFT.h"
#include "../include/colors.h"
#include "../include/Delays.h"


/*! hostFatalError() - Display fatal error detected by host */
/*! hostFtalError() flashes "Host Error" in a box with alternating
 *  colors.  The busy LED is also flashed.  The serial interrupt
 *  enable flag is cleared.
 * 
 * \param none
 * \return none
 */

void hostFatalError(void)
{
  int i;

  // Turn off the receive interrupt
  IFS1bits.U2EIF = 0;
  // Turn off the UART
  U2MODEbits.ON = 0;

  while (1)
    {
      TFTsetFont(BigFont);
      TFTclearRect(255,255,255, 70, 80, 220, 135);
      TFTsetColorX(RED);
      TFTrect(70,80,220,135);
      TFTsetBackColorX(WHITE);
      TFTprintChar('H',110,90);
      TFTprintChar('o',130,90);
      TFTprintChar('s',150,90);
      TFTprintChar('t',170,90);
      TFTprintChar('E', 100, 110);
      TFTprintChar('r', 120, 110);
      TFTprintChar('r', 140, 110);
      TFTprintChar('o', 160, 110);
      TFTprintChar('r', 180, 110);
      fatalErrorFlash();
      TFTsetFont(BigFont);
      TFTclearRect( 255,0,0, 70, 80, 220, 135);
      TFTsetColorX(WHITE);
      TFTrect(70,80,220,135);
      TFTsetBackColorX(RED);
      TFTprintChar('H',110,90);
      TFTprintChar('o',130,90);
      TFTprintChar('s',150,90);
      TFTprintChar('t',170,90);
      TFTprintChar('E', 100, 110);
      TFTprintChar('r', 120, 110);
      TFTprintChar('r', 140, 110);
      TFTprintChar('o', 160, 110);
      TFTprintChar('r', 180, 110);
      fatalErrorFlash();
    }
}
