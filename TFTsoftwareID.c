/*! \file  TFTsoftwareID.c
 *
 *  \brief Display a screen showing specific firmware version
 *
 *
 *  \author jjmcd
 *  \date June 19, 2015, 11:25 AM
 */
    /* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>
#include "../include/TFT.h"
#include "../include/colors.h"


/*! TFTsoftwareID() - Display a screen showing specific firmware version */

/*! TFTsoftwareID() erases the screen and displays the date and time of
 *  the current compile as well as the commit ID of the last release.
 *  This application includes manifest constants for these strings, so
 *  TFTsoftwareID() is typically called as:
\code
      TFTsoftwareID(__DATE__,__TIME__,COMMIT_ID);
\endcode
 * 
 * \param szDate char * - Compile date
 * \param szTime char * - Compile time
 * \param szCommit char * - Git commit hash of last release
 * \return none
 */
void TFTsoftwareID(char *szDate, char *szTime, char *szCommit )
{
  char szCompile[64];
  int nRed;
  short i;

  TFTsetBackColorX(BLACK);
  TFTclear();
  TFTsetBackColorX(DARKRED);
  TFTsetColorX(DARKRED);
  TFTfillRect(20,20,319-20,239-20);
  TFTfillRect(20,0,319-20,20);
  TFTfillRect(0,20,20,239-20);
  TFTfillRect(20,239-20,319-20,239);
  TFTfillRect(319-20,20,319,239-20);
  TFTfillCircle(20,20,21);
  TFTfillCircle(319-20,20,20);
  TFTfillCircle(20,239-20,21);
  TFTfillCircle(319-20,239-20,20);

  TFTsetColorX(0x7800);
  TFTfillRoundRect(46,46,286,206);
  TFTsetColorX(0x6000);
  TFTfillRoundRect(46,46,283,203);
  TFTsetColorX(0x58a1);
  TFTfillRoundRect(40,40,280,200);
  TFTsetColorX(PERU);
  TFTroundRect(40,40,280,200);
  TFTsetBackColorX(0x58a1);
  TFTsetColorX(YELLOW);
  TFTsetFont((fontdatatype*)BigFont);
  putString(160-3*12,50,12,"Serial");
  putString(160-4*13,66,13,"Graphics");
  putString(160-4*14,82,14,"Terminal");
  TFTsetFont((fontdatatype*)SmallFont);
  TFTsetColorX(SIENNA);
  putString(160-12*7-3,110,7,"John J. McDonough, WB8RCR");
  putString(160-28,171,7,"Revision:");
  putString(160-49,137,7,"Compile time:");
  TFTsetColorX(DARKSALMON);
  strcpy(szCompile,szDate);
  strcat(szCompile," ");
  strcat(szCompile,szTime);
  TFTsetFont((fontdatatype*)DJS);
  putString(70,150,9,szCompile);
  strcpy(szCompile,szCommit);
  szCompile[29]='\0';
  putString(45,183,8,szCompile);
}
