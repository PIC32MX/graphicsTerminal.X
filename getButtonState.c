/*! \file  getButtonState.c
 *
 *  \brief Check the state of a button
 *
 *  \author jjmcd
 *  \date July 17, 2015, 4:06 PM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/buttons.h"
#include "../Button.X/buttonAttributes.h"
#include "graphicsTerminal.h"

/*! getButtonState - Check the state of a button */

/*! getButtonState() sends 0x80 if rcButton[i].uState is TRUE,
 *  otherwise sends a 0x0
 * 
 * \param i int - Number of the button to check
 * \return none
 */
void getButtonState( int i )
{
  if ( rcButton[i].uState )
    szOutBuf[0] = 0x80;
  else
    szOutBuf[0] = 0x00;
  nOutLen = 1;
  serialOut();
}
