/*! \file  fatalError.c
 *
 *  \brief Displays a fatal error notification and halts
 *
 *
 *  \author jjmcd
 *  \date July 3, 2015, 9:32 PM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <string.h>
#include "../include/TFT.h"
#include "../include/colors.h"
#include "../include/Delays.h"



/*! fatalError() - Displays a fatal error notification and halts */
/*! fatalError() displays the word "Error" followed by a number
 *  within a box and flashes alternating colors.  It disables the serial
 *  interrupt preventingmore commands from being recognized and
 *  annoyingly flashes the busy LED.
 *
 * \param e int - Error number to display
 * \return none
 */
void fatalError(int e)
{
  int i;
  char *p;
  char szMessage[32],szNumber[4];

  itoa(szNumber,e,10);
  strcpy(szMessage,"Error ");
  strcat(szMessage,szNumber);

  // Turn off the receive interrupt
  IFS1bits.U2EIF = 0;
  // Turn off the UART
  U2MODEbits.ON = 0;

  while (1)
    {
      TFTsetFont(BigFont);
      TFTclearRect(255,255,255, 50, 80, 240, 130);
      TFTsetColorX(RED);
      TFTrect(50,80,240,130);
      TFTsetBackColorX(WHITE);
      i = 0;
      p=szMessage;
      while( *p )
        {
          TFTprintChar(*p,  90+15*i, 100);
          p++;
          i++;
        }
      fatalErrorFlash();
      TFTsetFont(BigFont);
      TFTclearRect( 255,0,0, 50, 80, 240, 130);
      TFTsetColorX(WHITE);
      TFTrect(50,80,240,130);
      TFTsetBackColorX(RED);
      i = 0;
      p=szMessage;
      while( *p )
        {
          TFTprintChar(*p,  90+15*i, 100);
          p++;
          i++;
        }
      fatalErrorFlash();
    }
}
